#!/bin/bash
unset -v DIAGNOSTICS ; # DIAGNOSTICS="-L debug" 
unset -v QUIC_OPTIONS ; QUIC_OPTIONS="-g -o cc_algo=1 -o idle_timeout=600 -o mtu_probe_timer=15000 -o noprogress_timeout=0 -o pace_packets=0 -o scid_iss_rate=0 -o send_prst=1" 
unset -v AUTHORITY ; AUTHORITY="jango-index.tk jango-index.ml www.jango-index.tk www.jango-index.ml" 
unset -v CHAIN ; CHAIN=/home/ubuntu/.acme.sh/jango-index.tk/fullchain.cer 
unset -v KEY ; KEY=/home/ubuntu/.acme.sh/jango-index.tk/jango-index.tk.key 
unset -v ADDRESS ; ADDRESS=:: 
unset -v PORT ; PORT=443 
unset -v CONNECT ; CONNECT=8080 
unset -v CERTIFICATE_OPTIONS ; for A in $AUTHORITY ; do CERTIFICATE_OPTIONS+="-c ${A},${CHAIN},${KEY} " ; done 
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}" ; lsquic/http_server-popen ${DIAGNOSTICS} ${QUIC_OPTIONS} -s ${ADDRESS}:${PORT} ${CERTIFICATE_OPTIONS} -t ${CONNECT} ) 
exit 0
