#!/bin/bash
unset -v DIAGNOSTICS ; # DIAGNOSTICS="-d -d" 
unset -v PORT ; PORT=4444 
unset -v CONNECT ; CONNECT=8080 
unset -v CERT ; CERT=/home/ubuntu/.acme.sh/ox-proxy.gq/ox-proxy.gq.cer 
unset -v KEY ; KEY=/home/ubuntu/.acme.sh/ox-proxy.gq/ox-proxy.gq.key 
unset -v CA ; CA=/home/ubuntu/.acme.sh/ox-proxy.gq/ca.cer 
unset -v DEFER_ACCEPT ; DEFER_ACCEPT=,defer-accept 
unset -v READ_BUFFER ; READ_BUFFER=,readbytes=1000000 
socat ${DIAGNOSTICS} -T 60 ssl-l:${PORT},pf=ip6,ipv6only=0,cert=${CERT},key=${KEY},cafile=${CA},verify=0,fork,keepalive,linger=60,nodelay${DEFER_ACCEPT}${READ_BUFFER},reuseaddr tcp:127.0.0.1:${CONNECT},keepalive,linger=60,nodelay 
exit 0
